# Getting started with LeasePlan assigment using Serenity and Cucumber 

##Installation
Pre requisite : clone the project from here :
      - https://gitlab.com/mbmdasilva/leaseplan-assigment/-/tree/main
Step 1:
  - Make sure that you have maven isntalled run mvn -version
  - Make sure you have jave jdk version 8 (1.8) to be able to run it locally
Step 2: Run the following maven commands to build your project
    - mvn dependency:resolve
    - mvn compile
    
##Run the tests  
How to: 
    - make sure that the runner class (CucumberRunner.class) is pointing to the feature file forder and a respective tag is passed under the tags
    - mvn clean verify -Denvironments={environemnt of choice we have local, dev and staging as you can see in the serenity.conf)
    - i.e.  [ -Denvironments=dev ]

##Add new tests
How to:
    - Add the scenarios for the feature to be tested in the feature file unde src/test/resources/features
    - Create the step definitions under src/test/java/starter/stepdefinions
    - Add the implementation for stepdefinitions in the step class under src/test/java/starter/steps
    - Once the tests are added , committed and pushed to the main branch a build job is triggered and run the tests to verify they all working as expected 

##Refactor
  - It was necessary to refactor the feature files as it was using the endpoints straight from the feature files it should go to serenity.conf file as baseUrl for different envs
  - Changed the positive scenarios for the product to scenario outline, because that allow us to reuse one stepdefinition for different products
  - Changed the assertions as we needed to check for both products that we are receiving a 200 status and then assert that the product title exists within the body
  - Added steps for Search products to best keep all the methods necessary for a particular class and be easier for maintainability, troubleshooting and even expanding coverage
  - Removed the CarAPI class not needed
  - Added domain classes and jackson library to better manipulate the json content
  - Cleaned up the pom.xml so it was not failing 
  - Removed all gradle files as we do not need it to run the tests also the project is supposed to be submitted as maven
  - Added a gitlab-ci.yml file and passed on the stages and scripts to build and run the tests and show the report

    


