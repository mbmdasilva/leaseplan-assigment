Feature: Search for the product

  @test
  Scenario Outline: check for products
    When he calls endpoint "/api/v1/search/test/" + <product>
    Then he sees the results displayed for <product>
    Examples:
      | product |
      | "apple" |
      | "tofu"  |
      | "mango" |
      | "water" |

  @test
  Scenario Outline: check for a product that is not available
    When he calls endpoint "/api/v1/search/test/" + <product>
    Then he does not see the results
    Examples:
      | product |
      | "car"   |
