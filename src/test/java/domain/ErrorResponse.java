package domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ErrorResponse {
    private boolean error;
    private String message;
    @JsonProperty("served_by")
    private String servedBy;
    @JsonProperty("requested_item")
    private String requestedItem;
}
