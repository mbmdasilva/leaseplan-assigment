package domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Product {
    private String provider;
    private String title;
    private String brand;
    private double price;
    private String unit;
    @JsonProperty("isPromo")
    private boolean isPromo;
    @JsonProperty("promoDetails")
    private String promoDetails;
    private String image;
    private String url;
}
