package starter.steps;

import domain.Detail;
import domain.ErrorResponse;
import domain.Product;
import io.restassured.response.Response;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.serenitybdd.rest.SerenityRest.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SearchApiSteps {
    EnvironmentVariables environmentVariables;
    public Response response;


    @Step
    public void getApiURProductName(String api, String productName) {
        String baseURL = EnvironmentSpecificConfiguration.from(environmentVariables)
                .getProperty("environments.local.baseurl");
        response = SerenityRest.given().get(baseURL + api + productName);
    }

    @Step
    public void verifyStatusCode(int expectedStatusCode) {
        restAssuredThat(response -> response.statusCode(expectedStatusCode));
    }

    @Step
    public void verifyTitle(String product) {
        Product[] resp = response.body().as(Product[].class);
        Stream<Product> products = Arrays.stream(resp);
        List<Product> prod = products.filter(item -> item.getTitle().trim().contains(product))
                .collect(Collectors.toList());
        assertTrue( prod.size()>0);
    }

    @Step
    public void noResultsDisplayed() {
        ErrorResponse expectedErrorResponse = ErrorResponse.builder().
                error(true).
                message("Not found").
                requestedItem("car").
                servedBy("https://waarkoop.com").
                build();
        Detail expectedDetail = Detail.builder().errorResponse(expectedErrorResponse).build();
        Detail actualDetail = response.as(Detail.class);
        assertEquals(expectedDetail,actualDetail);
    }
}
