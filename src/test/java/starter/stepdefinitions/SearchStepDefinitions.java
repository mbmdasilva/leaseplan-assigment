package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.apache.hc.core5.http.HttpStatus;
import starter.steps.SearchApiSteps;


public class SearchStepDefinitions {

    @Steps
    public SearchApiSteps searchApi;

    @When("he calls endpoint {string} + {string}")
    public void he_calls_endpoint(String api, String product) {
        searchApi.getApiURProductName(api, product);
    }

    @Then("he sees the results displayed for {string}")
    public void heSeesTheResultsDisplayedFor(String product) {
        searchApi.verifyStatusCode(HttpStatus.SC_OK);
        searchApi.verifyTitle(product);
    }

    @Then("he does not see the results")
    public void he_Does_Not_See_The_Results() {
        searchApi.verifyStatusCode(HttpStatus.SC_NOT_FOUND);
        searchApi.noResultsDisplayed();
    }

}
